package berlin.megagames.furyandfast;

import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

	public void start(Stage primaryStage) {

		Rennwagen racer = new Rennwagen();
		racer.setVmax(255);
		Garage garage = new Garage();
		World fury = new World(800, 600, primaryStage);
		fury.setName("Fast & Fury 0.1");
		fury.addObject(racer, fury.getWidth()/2, fury.getHeight()/2);
		fury.addObject(garage, 200, 200);		
	}

	public static void main(String argv[]) {
		launch(argv);
	}
}
