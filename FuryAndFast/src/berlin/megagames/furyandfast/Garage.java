package berlin.megagames.furyandfast;

import javafx.scene.image.Image;

/**
 * In der Garage kann genau ein Rennwagen geparkt werden
 * 
 * @author MegaGames Berlin
 * @version 0.1alpha
 */
public class Garage extends Actor {

	public Garage() {
		super(new Image("house.png"));
	}

	/**
	 * Parkt Rennwagen in die Garage
	 * 
	 * @param auto
	 *            Rennwagen, der geparkt wird
	 */
	public void parkeEin(Rennwagen auto) {
		System.out.println("Hier fehlt parkeEin()!");
	}

	/**
	 * Holt Wagen aus der Garage, wenn einer drin ist
	 * 
	 * @return Rennwagen, der aus der Garage geholt wird
	 */
	public Rennwagen parkeAus() {
		System.out.println("Hier fehlt parkeAus()!");
		return null;
	}

	/**
	 * Überprüft, ob die Garage besetzt ist
	 * 
	 * @return Besetzt-Status der Garage
	 */
	public boolean istBesetzt() {
		System.out.println("Hier fehlt istBesetzt()!");
		return false;
	}

	/**
	 * Verarbeitet Tastatureingabe --> Rennwagen ausparken bei Leertaste
	 * 
	 * TODO: dafür sorgen, dass wirklich ein Auto ein- und ausparken kann!
	 */
	public void act() {

		if (isKeyDown("Space")) {

			// Suche potentielles Auto ...
			Rennwagen autoAufGarage = (Rennwagen) getOneIntersectingObject(Rennwagen.class);
			
			// Tipps: 
			//   Auto auf Welt hinzufügen:
			//     this.getWorld().addObject(einAuto, einePosX, einePosY);
			//   Auto entfernen:
			//     this.getWorld().removeObject(einAuto);

			
			
			// Wenn Auto in Garage ist...
				// ...parke es aus!
			// Wenn kein Auto in Garage ist...
					// ... und auf Garage steht
			// ...parke es ein!
		}

	}
}
